FROM alpine:3.19

RUN apk update && apk add python3

ADD --chown=root:root --chmod=550 dotprops2json /usr/local/bin/
