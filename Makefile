# Assume venv; for dev only

.PHONY: init test style build

PROJECT_NAME := $(shell basename "$(CURDIR)")

all: style test

# Set up a new venv
init:
	rm -rf .venv
	python3.11 -m venv .venv
	.venv/bin/pip install --upgrade pip poetry

# Run unit tests and report coverage
test:
	.venv/bin/python -m coverage run --source=$(PROJECT_NAME) -m unittest
	.venv/bin/python -m coverage report

# Check code style - use AutoPEP8 to clean up some
style:
	.venv/bin/autopep8 -ir .
	.venv/bin/pycodestyle $(PROJECT_NAME)
	.venv/bin/pycodestyle test

build:
	rm -rf dist
	.venv/bin/poetry build